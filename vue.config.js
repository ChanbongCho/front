const CompressionPlugin = require("compression-webpack-plugin");
module.exports = {
  chainWebpack(config) {
    // 정적 파일 압축. 글꼴파일때문에 씀
    config.plugin("CompressionPlugin").use(CompressionPlugin);
  }
};
