import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import VueAxios from "vue-axios";

import "./assets/css/reset.css";
import "./assets/css/main.css";
import Dialog from "@/components/global/Dialog";

Vue.config.productionTip = false;
Vue.component("Modal", Dialog);
Vue.use(VueAxios, axios);

const observableDB = Vue.observable({ DB: {} });

Object.defineProperty(Vue.prototype, "$DB", {
  get() {
    return observableDB.DB;
  },
  set(value) {
    observableDB.DB = value;
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
