// const department = [
//   {
//     id: 1,
//     ord: 1,
//     type: "internal-medicine",
//     title_korean: "내과",
//     description:
//       "내과는 서양의학의 중요한 한 축이라고 할 수 있습니다.<br>보통은 수술적 치료를 제외한 모든 질환의 진단이나 치료를 내과에서 담당합니다.<br>그러나 최근에는 치료 내시경 등을 시행하여 수술을 하지 않고도 내시경을 이용하여 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/internal-medicine.svg",
//     position: {
//       x: 70,
//       y: 89
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "손미정",
//         title: "1내과 과장",
//         major: "소화기",
//         education: [
//           {
//             ord: 1,
//             value: "경상대학교 의과대학 졸업"
//           }
//         ],
//         career: [
//           {
//             ord: 1,
//             value: "경상대학교 병원 인턴, 레지던트 수료"
//           },
//           {
//             ord: 2,
//             value: "경상대학교 병원 전임의(임상강사)"
//           },
//           {
//             ord: 3,
//             value: "현) 마산의료원 1내과 과장"
//           }
//         ],
//         academy: [
//           {
//             ord: 1,
//             value: "대한 결핵 호흡기 학회 회원"
//           },
//           {
//             ord: 2,
//             value: "대한 천식 알레르기 학회 회원"
//           },
//           {
//             ord: 3,
//             value: "대한 내과 학회 회원"
//           },
//           {
//             ord: 4,
//             value: "천식, 폐렴, 만성폐쇄성 폐질환 연구"
//           }
//         ]
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "임수진",
//         title: "2내과 과장",
//         major: "호흡기내과",
//         education: [
//           {
//             ord: 1,
//             value: "경상대학교 의과대학 졸업"
//           }
//         ],
//         career: [
//           {
//             ord: 1,
//             value: "경상대학교 병원 인턴, 레지던트 수료"
//           },
//           {
//             ord: 2,
//             value: "경상대학교 병원 전임의(임상강사)"
//           },
//           {
//             ord: 3,
//             value: "현) 마산의료원 2내과 과장"
//           }
//         ],
//         academy: [
//           {
//             ord: 1,
//             value: "대한 결핵 호흡기 학회 회원"
//           },
//           {
//             ord: 2,
//             value: "대한 천식 알레르기 학회 회원"
//           },
//           {
//             ord: 3,
//             value: "대한 내과 학회 회원"
//           },
//           {
//             ord: 4,
//             value: "천식, 폐렴, 만성폐쇄성 폐질환 연구"
//           }
//         ]
//       },
//       {
//         id: 3,
//         ord: 3,
//         picture: "",
//         name: "전대홍",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [
//           {
//             ord: 1,
//             value: "경상대학교 의과대학 졸업"
//           }
//         ],
//         career: [
//           {
//             ord: 1,
//             value: "경상대학교 병원 인턴, 레지던트"
//           },
//           {
//             ord: 2,
//             value: "경상대학교 병원 신장내과 임상강사"
//           },
//           {
//             ord: 3,
//             value: "경상대학교병원 대학원 박사과정 신장투석 전문의"
//           },
//           {
//             ord: 4,
//             value: "현) 마산의료원 3내과 과장"
//           }
//         ],
//         academy: [
//           {
//             ord: 1,
//             value: "대한내과학회 정회원"
//           },
//           {
//             ord: 2,
//             value: "대한신장학회 정회원"
//           },
//           {
//             ord: 3,
//             value: "신장, 신장투석, 당뇨, 고혈압, 고지혈증"
//           }
//         ]
//       },
//       {
//         id: 4,
//         ord: 4,
//         picture: "",
//         name: "권정훈",
//         title: "5내과 과장",
//         major: "소화기, 내분비(당뇨병, 갑상선 질환)",
//         education: [
//           {
//             ord: 1,
//             value: "경북대학교 의과대학 졸업"
//           }
//         ],
//         career: [
//           {
//             ord: 1,
//             value: "마산삼성병원 인턴"
//           },
//           {
//             ord: 2,
//             value: "마산삼성병원 레지던트"
//           },
//           {
//             ord: 3,
//             value: "마산청아병원 내과 과장"
//           },
//           {
//             ord: 4,
//             value: "현) 마산의료원 5내과 과장"
//           }
//         ],
//         academy: []
//       },
//       {
//         id: 5,
//         ord: 5,
//         picture: "",
//         name: "배인규",
//         title: "8내과 과장",
//         major: "감염질환",
//         education: [
//           {
//             ord: 1,
//             value: "서울대학교 의과대학 졸업"
//           }
//         ],
//         career: [
//           {
//             ord: 1,
//             value: "서울대학교 의과대학 졸업"
//           },
//           {
//             ord: 2,
//             value: "서울아산병원 내과 전공의"
//           },
//           {
//             ord: 3,
//             value: "서울아산병원 감염내과 임상강사"
//           },
//           {
//             ord: 4,
//             value: "을지의과대학 서울을지병원 내과 교수"
//           },
//           {
//             ord: 5,
//             value: "현재 경상대학교병원 감염내과 교수"
//           },
//           {
//             ord: 6,
//             value: "미국 Duke University Medical Center 연수"
//           }
//         ],
//         academy: [
//           {
//             ord: 1,
//             value: "대한감염학회"
//           },
//           {
//             ord: 2,
//             value: "대한화학요법학회"
//           },
//           {
//             ord: 3,
//             value: "대한의료관련감염학회"
//           },
//           {
//             ord: 4,
//             value: "대한임상미생물학회"
//           },
//           {
//             ord: 5,
//             value: "대한백신학회"
//           }
//         ]
//       }
//     ]
//   },
//   {
//     id: 2,
//     ord: 2,
//     type: "neurology",
//     title_korean: "신경과",
//     description:
//       "신경과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/neurology.svg",
//     position: {
//       x: 57,
//       y: 93
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "김신경",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "강신경",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 3,
//     ord: 3,
//     type: "surgery",
//     title_korean: "외과",
//     description:
//       "외과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/surgery.svg",
//     position: {
//       x: 60,
//       y: 87
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "홍길동",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "김삼순",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 4,
//     ord: 4,
//     type: "orthopedics",
//     title_korean: "정형외과",
//     description:
//       "정형외과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/orthopedics.svg",
//     position: {
//       x: 52,
//       y: 108
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "하지원",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "김명민",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 5,
//     ord: 5,
//     type: "neurosurgery",
//     title_korean: "신경외과",
//     description:
//       "신경외과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/neurosurgery.svg",
//     position: {
//       x: 55,
//       y: 88
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "우태하",
//         title: "17신경외과 과장",
//         major: "경추,요추디스크질환",
//         education: [
//           {
//             ord: 1,
//             value: "경상대학교 의과대학 졸업"
//           },
//           {
//             ord: 2,
//             value: "경상대학교 의과대학원 수료"
//           }
//         ],
//         career: [
//           {
//             ord: 1,
//             value: "경상대학교 병원 인턴 수료"
//           },
//           {
//             ord: 2,
//             value: "경상대학교 병원 신경외과 레지던트 수료"
//           },
//           {
//             ord: 3,
//             value: "신경외과 전문의"
//           },
//           {
//             ord: 4,
//             value: "현 마산의료원 신경외과 과장"
//           }
//         ],
//         academy: [
//           {
//             ord: 1,
//             value: "대한 신경외과 학회 정회원"
//           },
//           {
//             ord: 2,
//             value: "대한 신경외과 척추학회 정회원"
//           }
//         ]
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "황시목",
//         title: "18신경외과 과장",
//         major: "뇌혈관질환, 척추질환",
//         education: [
//           {
//             ord: 1,
//             value: "경상대학교 의과대학 졸업"
//           },
//           {
//             ord: 2,
//             value: "인제대학교 의학박사"
//           }
//         ],
//         career: [
//           {
//             ord: 1,
//             value: "육군 을지부대 52연대 4지대장"
//           },
//           {
//             ord: 2,
//             value: "경상대학교 병원 신경외과 전문의 수료"
//           },
//           {
//             ord: 3,
//             value: "서울삼성병원 신경외과 임상강사 수료"
//           },
//           {
//             ord: 4,
//             value: "경상대학교병원 신경외과 임상강사 수료"
//           },
//           {
//             ord: 5,
//             value: "경상대학교병원 전임교원 임용"
//           },
//           {
//             ord: 6,
//             value: "미국 클리브랜드 클리닉 척추연구소 연수"
//           },
//           {
//             ord: 7,
//             value: "현) 창원경상대학교병원 신경외과 교수"
//           },
//           {
//             ord: 8,
//             value: "현) 경상남도 마산의료원 원장"
//           }
//         ],
//         academy: [
//           {
//             ord: 1,
//             value: "대한신경외과 학회"
//           },
//           {
//             ord: 2,
//             value: "대한 척추신경외과 학회"
//           },
//           {
//             ord: 3,
//             value: "대한 뇌혈관 학회"
//           }
//         ]
//       }
//     ]
//   },
//   {
//     id: 6,
//     ord: 6,
//     type: "rehabilitation",
//     title_korean: "재활의학과",
//     description:
//       "재활의학과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/rehabilitation.svg",
//     position: {
//       x: 47,
//       y: 82
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "최빛",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "한여진",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 7,
//     ord: 7,
//     type: "emergency-medicine",
//     title_korean: "응급의학과",
//     description:
//       "응급의학과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/emergency-medicine.svg",
//     position: {
//       x: 56,
//       y: 80
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "장건",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "김순창",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 8,
//     ord: 8,
//     type: "obstetrics-gynecology",
//     title_korean: "산부인과",
//     description:
//       "산부인과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/obstetrics-gynecology.svg",
//     position: {
//       x: 57,
//       y: 84
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "강강강",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "다다다",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 9,
//     ord: 9,
//     type: "pediatrics-and-adolescent-medicine",
//     title_korean: "소아청소년과",
//     description:
//       "소아청소년과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/pediatrics-and-adolescent-medicine.svg",
//     position: {
//       x: 44,
//       y: 97
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "문소리",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "문상태",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 10,
//     ord: 10,
//     type: "otolaryngology",
//     title_korean: "이비인후과",
//     description:
//       "이비인후과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/otolaryngology.svg",
//     position: {
//       x: 52,
//       y: 100
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "코훌쩍",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "김켁켁",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 11,
//     ord: 11,
//     type: "anesthesiology-pain-medicine",
//     title_korean: "마취통증의학과",
//     description:
//       "마취통증의학과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/anesthesiology-pain-medicine.svg",
//     position: {
//       x: 51,
//       y: 77
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "김패인",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "정해인",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 12,
//     ord: 12,
//     type: "urology",
//     title_korean: "비뇨의학과",
//     description:
//       "비뇨의학과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/urology.svg",
//     position: {
//       x: 69,
//       y: 95
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "이부진",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "이재용",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 13,
//     ord: 13,
//     type: "thoracic",
//     title_korean: "흉부외과",
//     description:
//       "흉부외과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/thoracic.svg",
//     position: {
//       x: 50,
//       y: 86
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "베토벤",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "모차르트",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 14,
//     ord: 14,
//     type: "psychiatry",
//     title_korean: "정신건강의학과",
//     description:
//       "정신건강의학과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/psychiatry.svg",
//     position: {
//       x: 53,
//       y: 95
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "프시케",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "에로스",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 15,
//     ord: 15,
//     type: "diagnosis",
//     title_korean: "진단검사의학과",
//     description:
//       "진단검사의학과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/diagnosis.svg",
//     position: {
//       x: 77,
//       y: 85
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "이하늬",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "김태희",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 16,
//     ord: 16,
//     type: "interventional-radiology",
//     title_korean: "영상의학과",
//     description:
//       "영상의학과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/interventional-radiology.svg",
//     position: {
//       x: 60,
//       y: 94
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "전지현",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "송혜교",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   },
//   {
//     id: 17,
//     ord: 17,
//     type: "dermatology",
//     title_korean: "피부과",
//     description:
//       "피부과입니다.<br>보통은 신경과에서 담당합니다.<br>그러나 최근에는 치료를 하는 경우도 있습니다.",
//     location: "본관 1F",
//     img: "/img/department_list/dermatology.svg",
//     position: {
//       x: 56,
//       y: 85
//     },
//     doctors: [
//       {
//         id: 1,
//         ord: 1,
//         picture: "",
//         name: "고문영",
//         title: "3내과 과장",
//         major: "신장 투석",
//         education: [],
//         career: [],
//         academy: []
//       },
//       {
//         id: 2,
//         ord: 2,
//         picture: "",
//         name: "문강태",
//         title: "5내과 과장",
//         major: "내분비(당뇨병, 갑상선 질환)",
//         education: [],
//         career: [],
//         academy: []
//       }
//     ]
//   }
// ];
import dep from './department.json'
export const mockData = {
  password: 123456,
  setting: {
    head_volume: 70,
    body_volume: 0,
    day: "08:00-17:30",
    night: "18:00-05:50",
    patrol: "22:00-23:30--04:00",
    patrol_flag: "on-on-off-on",
    temp: "under-37",
    timeoutSeconds: 15
  },
  // department: department
  department: dep.department
};
