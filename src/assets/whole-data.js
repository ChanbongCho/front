class Location {
  constructor(name, floor, number, position) {
    this.name = name;
    this.floor = floor;
    this.number = number;
    this.position = position;
  }
}

const maps = [
  {
    floor: "B1F",
    imgPath: ""
  },
  {
    floor: "1F",
    imgPath: ""
  },
  {
    floor: "2F",
    imgPath: ""
  },
  {
    floor: "3F",
    imgPath: ""
  },
  {
    floor: "4F",
    imgPath: ""
  },
  {
    floor: "5F",
    imgPath: ""
  }
];

const locations = [
  new Location("직원 식당", "B1F", "01", { top: 554, left: 510 }),
  new Location("노조 사무실", "B1F", "02", { top: 372, left: 359 }),
  new Location("강당", "B1F", "03", { top: 424, left: 305 }),
  new Location("시설팀", "B1F", "04", { top: 267, left: 838 }),
  new Location("영양실", "B1F", "05", { top: 449, left: 851 }),
  new Location("공공의료본부", "B1F", "06", { top: 498, left: 231 }),

  new Location("신경과", "1F", "01", { top: 436, left: 84 }),
  new Location("정형외과", "1F", "02", { top: 529, left: 176 }),
  new Location("내과", "1F", "03", { top: 551, left: 788 }),
  new Location("산부인과", "1F", "04", { top: 552, left: 929 }),
  new Location("내시경실", "1F", "05", { top: 461, left: 777 }),
  new Location("건강증진센터", "1F", "06", { top: 329, left: 975 }),
  new Location("약제과", "1F", "07", { top: 390, left: 245 }),
  new Location("원무과", "1F", "08", { top: 369, left: 404 }),
  new Location("주사실", "1F", "09", { top: 455, left: 215 }),
  new Location("응급실<br>(응급의학과)", "1F", "10", { top: 286, left: 356 }),
  new Location("영상의학과", "1F", "11", { top: 286, left: 692 }),
  new Location("커피숍", "1F", "12", { top: 552, left: 593 }),
  new Location("접수/수납/<br>제증명/입퇴원", "1F", "13", {
    top: 446,
    left: 452
  }),
  new Location("채혈실", "1F", "14", { top: 374, left: 687 }),

  new Location("소아청소년과", "2F", "01", { top: 460, left: 84 }),
  new Location("외과", "2F", "02", { top: 514, left: 135 }),
  new Location("이비인후과", "2F", "03", { top: 570, left: 191 }),
  new Location("재활의학과", "2F", "04", { top: 581, left: 672 }),
  new Location("신경외과", "2F", "05", { top: 581, left: 758 }),
  new Location(
    "비뇨의학과/<br>흉부외과/<br>정신건강의학과/<br>피부과",
    "2F",
    "06",
    {
      top: 581,
      left: 832
    }
  ),
  new Location("인공신장실", "2F", "07", { top: 415, left: 237 }),
  new Location("진단검사의학과", "2F", "08", { top: 241, left: 431 }),
  new Location("재활치료실", "2F", "09", { top: 311, left: 709 }),
  new Location("작업치료실", "2F", "10", { top: 364, left: 594 }),
  new Location("편의점", "2F", "11", { top: 582, left: 506 }),
  new Location("간호교육실", "2F", "12", { top: 351, left: 350 }),
  new Location("영양상담실", "2F", "13", { top: 392, left: 390 }),
  new Location("총무과", "2F", "14", { top: 270, left: 852 }),
  new Location("기획예산과", "2F", "15", { top: 382, left: 980 }),
  new Location("감염관리실", "2F", "16", { top: 338, left: 841 }),
  new Location("간호부", "2F", "17", { top: 482, left: 854 }),
  new Location("보험심사실/<br>의료정보실", "2F", "18", {
    top: 389,
    left: 841
  }),
  new Location("회의실", "2F", "19", { top: 353, left: 1030 }),
  new Location("전산정보실", "2F", "20", { top: 481, left: 721 }),
  new Location("적정관리실", "2F", "21", { top: 389, left: 773 }),
  new Location("수납창구", "2F", "22", { top: 502, left: 435 }),

  new Location("31병동", "3F", "01", { top: 292, left: 382 }),
  new Location("옥상정원 3F", "3F", "02", { top: 456, left: 221 }),
  new Location("데이룸 3F", "3F", "03", { top: 556, left: 491 }),
  new Location("중환자실", "3F", "04", { top: 540, left: 670 }),
  new Location("중앙공급실", "3F", "05", { top: 540, left: 872 }),
  new Location("수술실", "3F", "06", { top: 290, left: 838 }),

  new Location("41병동", "4F", "01", { top: 304, left: 333 }),
  new Location("42병동", "4F", "02", { top: 505, left: 772 }),
  new Location("데이룸 4F", "4F", "03", { top: 591, left: 451 }),
  new Location("옥상정원 4F", "4F", "04", { top: 286, left: 894 }),

  new Location("51병동", "5F", "01", { top: 284, left: 396 }),
  new Location("52병동", "5F", "02", { top: 493, left: 819 }),
  new Location("호스피스<br>완화의료병동", "5F", "03", { top: 551, left: 965 }),
  new Location("격리병동", "5F", "04", { top: 340, left: 291 }),
  new Location("데이룸 5F", "5F", "05", { top: 570, left: 507 })
];

export const DB = {
  locations: locations,
  maps: maps,
  power: "",
  isConnected: false,
  isDataSet: false,
  isDialogShow: false,
  isTimeOut: false,
  isScreenTouched: false,
  prevent: false,

  settingUpdated: false,

  emergency: false,
  fireDetected: false,
  confirm: false,

  // 초성검색
  initialInput: "",
  hasNoInitialResult: false,
  initialResultArray: [],

  pictureFaceChanged: false,
  pictureCostumeChanged: false,
  fiveSecondsLeft: false,
  requestTimeMore: false,

  // 화면 클릭해서 타이머 막기 관련된 변수
  stopForPopup: true,
  stopForPageMove: true,

  // 브릿지에서 보내주는 데이터는 기본값 지정
  avatar: {
    monitor: "BASIC",
    // patrol: "SUNGLASSES",
    picture: "SMILE",
    costume: ""
  },
  password: "",
  setting: {
    head_volume: 0,
    body_volume: 0,
    day: "",
    night: "",
    patrol: "",
    patrol_flag: "",
    temp: "",
    timeoutSeconds: 0,
  },
  tempSetting: {
    head_volume: 0,
    body_volume: 0,
    day: "",
    night: "",
    patrol: "",
    patrol_flag: "",
    temp: "",
    timeoutSeconds: 0
  },
  department: []
};
