import Vue from "vue";
import VueRouter from "vue-router";
import BaseContentsContainer from "@/components/global/BaseContentsContainer";
import AdminIndex from "@/views/admin/AdminIndex";
import AdminBoot from "@/views/boot/AdminBoot";
import AdminFrontPage from "@/views/admin/AdminFrontPage";
import SettingPage from "@/views/admin/SettingPage";
import MainIndex from "@/views/main/MainIndex";
import MedicalInformationIndex from "@/views/main/medical-information/MedicalInformationIndex";
import MedicalInformationList from "@/views/main/medical-information/MedicalInformationList";
import DepartmentIndex from "@/views/main/medical-information/department/DepartmentIndex";
import DepartmentList from "@/views/main/medical-information/department/DepartmentList";
import TreatmentIndex from "@/views/main/medical-information/treatment/TreatmentIndex";
import DepartmentDetail from "@/views/main/medical-information/department/DepartmentDetail";
import MainList from "@/views/main/MainList";
import TreatmentList from "@/views/main/medical-information/treatment/TreatmentList";
import OfficeHours from "@/views/main/medical-information/treatment/OfficeHours";
import Procedure from "@/views/main/medical-information/treatment/Procedure";
import Emergency from "@/views/main/medical-information/treatment/Emergency";
import HospitalizationIndex from "@/views/main/medical-information/hospitalization/HospitalizationIndex";
import HospitalizationList from "@/views/main/medical-information/hospitalization/HospitalizationList";
import HospitalizationEnter from "@/views/main/medical-information/hospitalization/HospitalizationEnter";
import HospitalizationDischarge from "@/views/main/medical-information/hospitalization/HospitalizationDischarge";
import VisitorGuide from "@/views/main/medical-information/visitor/VisitorGuide";
import UseInformationIndex from "@/views/main/use-information/UseInformationIndex";
import UseInformationList from "@/views/main/use-information/UseInformationList";
import LocationIndex from "@/views/main/use-information/location/LocationIndex";
import LocationFloorList from "@/views/main/use-information/location/LocationFloorList";
import LocationSearch from "@/views/main/use-information/location/LocationSearch";
import ParkingGuide from "@/views/main/use-information/parking/ParkingGuide";
import CertificateGuide from "@/views/main/use-information/certificate/CertificateGuide";
import TakingPicturePage from "@/views/main/take-picture/TakingPicturePage";
import VoiceSearchPage from "@/views/main/voice-search/VoiceSearchPage";
import TemperatureSetting from "@/views/admin/TemperatureSetting";
import ScheduleSetting from "@/views/admin/ScheduleSetting";
import MonitorPage from "@/views/MonitorPage";
import PatrolMove from "@/views/patrol/PatrolMove";
import PatrolCodeScan from "@/views/patrol/PatrolCodeScan";
import PatrolFireDetection from "@/views/patrol/PatrolFireDetection";
import PatrolTakePicture from "@/views/patrol/PatrolTakePicture";
import PatrolIndex from "@/views/patrol/PatrolIndex";
import SelectFace from "@/views/main/take-picture/SelectFace";
import SelectCostume from "@/views/main/take-picture/SelectCostume";
import SelectedAvatar from "@/views/main/take-picture/SelectedAvatar";
import BatterySetting from "@/views/admin/BatterySetting";
import SoundSetting from "@/views/admin/SoundSetting";
import AdminBootLoading from "@/views/boot/AdminBootLoading";
import AdminAuthentication from "@/views/boot/AdminAuthentication";

const routes = [
  {
    path: "/",
    name: "Home",
    redirect: "/boot",
    component: BaseContentsContainer,
    children: [
      {
        path: "boot",
        name: "",
        component: AdminBoot,
        children: [
          {
            path: "",
            name: "Boot Page",
            component: AdminBootLoading
          },
          {
            path: "password",
            name: "Boot Password Page",
            component: AdminAuthentication
          }
        ]
      },
      {
        path: "admin",
        name: "Admin Page",
        component: AdminIndex,
        children: [
          {
            path: "front",
            name: "Admin Front Page",
            component: AdminFrontPage
          },
          {
            path: "setting",
            component: SettingPage,
            children: [
              {
                path: "",
                name: "Setting Page",
                redirect: "/admin/setting/battery"
              },
              {
                path: "battery",
                name: "Battery Setting Page",
                component: BatterySetting
              },
              {
                path: "sound",
                name: "Sound Setting Page",
                component: SoundSetting
              },
              {
                path: "temperature-range",
                name: "Body Temperature Range Setting Page",
                component: TemperatureSetting
              },
              {
                path: "scenario-schedule",
                name: "Scenario Schedule Setting Page",
                component: ScheduleSetting
              }
            ]
          }
        ]
      },
      {
        path: "main",
        component: MainIndex,
        children: [
          {
            path: "",
            name: "Main List Page",
            component: MainList
          },
          {
            path: "medical-information",
            component: MedicalInformationIndex,
            children: [
              {
                path: "",
                name: "Medical Information List",
                component: MedicalInformationList
              },
              {
                path: "department",
                component: DepartmentIndex,
                children: [
                  {
                    path: "",
                    name: "Medical Department List",
                    component: DepartmentList
                  },
                  {
                    path: ":name",
                    name: "Medical Department detail",
                    component: DepartmentDetail
                  }
                ]
              },
              {
                path: "treatment",
                component: TreatmentIndex,
                children: [
                  {
                    path: "",
                    name: "Treatment List",
                    component: TreatmentList
                  },
                  {
                    path: "office-hours",
                    name: "Treatment Office Hours",
                    component: OfficeHours
                  },
                  {
                    path: "procedure",
                    name: "Treatment Procedure",
                    component: Procedure
                  },
                  {
                    path: "emergency",
                    name: "Emergency Treatment",
                    component: Emergency
                  }
                ]
              },
              {
                path: "hospitalization",
                component: HospitalizationIndex,
                children: [
                  {
                    path: "",
                    name: "Hospitalization List",
                    component: HospitalizationList
                  },
                  {
                    path: "enter",
                    name: "Hospitalization Enter",
                    component: HospitalizationEnter
                  },
                  {
                    path: "discharge",
                    name: "Hospitalization Discharge",
                    component: HospitalizationDischarge
                  }
                ]
              },
              {
                path: "visit",
                name: "Visitor Guide",
                component: VisitorGuide
              }
            ]
          },
          {
            path: "use-information",
            component: UseInformationIndex,
            children: [
              {
                path: "",
                name: "Use Information List",
                component: UseInformationList
              },
              {
                path: "location",
                component: LocationIndex,
                children: [
                  {
                    path: "",
                    name: "Location Floor List",
                    component: LocationFloorList
                  },
                  {
                    path: "search",
                    name: "Location Search",
                    component: LocationSearch
                  }
                ]
              },
              {
                path: "parking",
                name: "Parking Guide",
                component: ParkingGuide
              },
              {
                path: "certificate",
                name: "Certificate Guide",
                component: CertificateGuide
              }
            ]
          },
          {
            path: "voice-search",
            name: "Voice Search Page",
            component: VoiceSearchPage
          },
          {
            path: "take-picture",
            component: TakingPicturePage,
            redirect: "take-picture/select-avatar",
            children: [
              {
                path: "select-avatar",
                name: "Select Avatar",
                component: SelectFace
              },
              {
                path: "select-dress",
                name: "Select Dress",
                component: SelectCostume
              },
              {
                path: "shot-avatar",
                name: "Shot Avatar",
                component: SelectedAvatar
              }
            ]
          }
        ]
      },
      {
        path: "monitor/:faceType?",
        name: "Monitor Page",
        component: MonitorPage
      },
      {
        path: "patrol",
        component: PatrolIndex,
        children: [
          {
            path: "",
            name: "Patrol",
            component: PatrolMove
          },
          {
            path: "code-scan",
            name: "Patrol Code Scan",
            component: PatrolCodeScan
          },
          {
            path: "fire-detection",
            name: "Patrol Fire Detection",
            component: PatrolFireDetection
          },
          {
            path: "take-picture",
            name: "Patrol Take Picture",
            component: PatrolTakePicture
          }
        ]
      }
    ]
  },
  { path: "*", component: AdminBoot }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

Vue.use(VueRouter);

export default router;
