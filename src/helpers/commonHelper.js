// 한글 -> 유니코드 변환
export function replaceAll(strTemp, strValue1, strValue2) {
    const t = true
    while (t) {
        if (strTemp.indexOf(strValue1) !== -1)
            strTemp = strTemp.replace(strValue1, strValue2);
        else
            break;
    }
    return strTemp;
}

export default {
    replaceAll
}
