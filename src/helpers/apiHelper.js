// 인스턴스 준비
// ----------------------
import ROSLIB from "roslib";
import {cleatAllInterval, EVENTBUS, EVENTS} from "@/helpers/eventHelper";
import {replaceAll} from "@/helpers/commonHelper";

export let ros

export const SUB = {
    fromBridge: '',
    setView: '',
    setAvatar: '',
    batteryStatus: '',
    chargingStatus: '',
}
export const PUB = {
    status: '',
    toBridge: ''
}

/* =================================
*     전역 수신을 위한 토픽 구독 이벤트
* ================================== */

// 토픽 구독(최상위)
// ----------------------
function regTopicFromBridge() {
    SUB.fromBridge.subscribe(function (message) {
        const unescapedStr = unescape(replaceAll(message.data, "\\", "%"))
        EVENTBUS.$emit(EVENTS.RECEIVE.BRIDGE_PROCESS, unescapedStr)
    })
}

function regTopicBatteryStatus() {
    SUB.batteryStatus.subscribe(function (message) {
        console.log('battery_status', message);
        EVENTBUS.$emit(EVENTS.RECEIVE.BATTERY_STATUS, message)
    });
}

function regTopicChargingStatus() {
    SUB.chargingStatus.subscribe(function (message) {
        console.log('charging', message);
        EVENTBUS.$emit(EVENTS.RECEIVE.CHARGING_STATUS, message)
    });
}

function regTopicSetView() {
    SUB.setView.subscribe(function (message) {
        console.log('set_view', message);
        EVENTBUS.$emit(EVENTS.RECEIVE.SET_VIEW, message)
    });
}

// 서버에서 요청받은 페이지로 이동하여, 요청받은 아바타 표정을 설정함
function regTopicSetAvatar() {
    SUB.setAvatar.subscribe(function (message) {
        console.log('set_avatar', message);
        EVENTBUS.$emit(EVENTS.RECEIVE.SET_AVATAR, message)
    })
}

/* =================================
*                설정
* ================================== */
let appVM = {}

export function prepareAPI(context) {
    appVM = context
    console.log('모드 : ', process.env.NODE_ENV)

    // 지속적 커넥션 시도 & 되면 시도 중단
    appVM.intervalId = setInterval(() => {
        if (!appVM.isConnected) {
            makeConnectionResponse();
        } else {
            cleatAllInterval()
        }
    }, 1000)
}

function makeConnectionResponse() {
    // @Todo 추후 production 주소에 맞게 바꿔야 함
    if (process.env.NODE_ENV === "production") {
        ros = new ROSLIB.Ros({url: "ws://125.141.87.158:8082"});
    } else if (process.env.NODE_ENV === "development") {
        ros = new ROSLIB.Ros({url: "ws://localhost:9090"});
    }

    ros.on('connection', function () {
        console.log('접속된')
        appVM.isConnected = true;
        EVENTBUS.$emit(EVENTS.RECEIVE.CONNECTION);

        // 접속 됐을 때만 관련 토픽 동록
        prepareTopicConnection()
        regTopicSetView();
        regTopicSetAvatar();
        regTopicFromBridge();
        regTopicBatteryStatus();
        regTopicChargingStatus();
    });

    ros.on('error', function (error) {
        console.log(error);
    });

    ros.on('close', function () {
        appVM.isConnected = false;
        console.log('closed');
    });
}

function prepareTopicConnection() {

    // 토픽 구독
    // ----------------------
    // 브릿지 -> 프론트
    SUB.fromBridge = new ROSLIB.Topic({
        ros: ros,
        name: '/dt/front_end/bridge_to_front_end',
        messageType: 'std_msgs/String'
    });

    // 서버에서 요청하는 특정 페이지로 이동
    SUB.setView = new ROSLIB.Topic({
        ros: ros,
        name: '/dt/front_end/set_view',
        messageType: 'std_msgs/String'
    });

    // 배터리 수치 0~100 수신
    SUB.batteryStatus = new ROSLIB.Topic({
        ros: ros,
        name: '/dt/front_end/battery_status',
        messageType: 'std_msgs/Int32'
    });

    // 도킹(충전중) 언도킹 상태 불리언 수신
    SUB.chargingStatus = new ROSLIB.Topic({
        ros: ros,
        name: '/dt/front_end/charging_status',
        messageType: 'std_msgs/Bool'
    });

    // 대기/순찰 화면에서 보여줄 아바타 표정값 수신
    SUB.setAvatar = new ROSLIB.Topic({
        ros: ros,
        name: '/dt/front_end/set_avatar',
        messageType: 'std_msgs/String'
    });

    // 토픽 발송
    // ----------------------
    // 상태 발송
    PUB.status = new ROSLIB.Topic({
        ros: ros,
        name: '/dt/front_end/status',
        messageType: 'std_msgs/String'
    });

    // 프론트 -> 브릿지
    PUB.toBridge = new ROSLIB.Topic({
        ros: ros,
        name: '/dt/front_end/front_end_to_bridge',
        messageType: 'std_msgs/String'
    });
}

export default {
    prepareAPI,
    PUB,
    SUB
}

