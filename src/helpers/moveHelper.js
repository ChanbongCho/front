// 박스 선택 여부값 변경
function setActiveClass(status, vueInstance) {
  vueInstance[status] = true;
}

// 0.3초 후 라우터 패스 이동
function moveAfterDelay(path, vueInstance) {
  setTimeout(() => {
    vueInstance.$DB.stopForPageMove = true
    vueInstance.$router.push(path);
  }, 300);
}

// 박스 선택 시 active 클래스 적용하고 0.3초 후 라우터 패스 이동
function selectAndDelayMove(path, status, vueInstance) {
  this.setActiveClass(status, vueInstance);
  this.moveAfterDelay(path, vueInstance);
}

// 0.3초 후 인자로 받은 메소드 실행
function doAfterDelay(method) {
  setTimeout(() => {
    method();
  }, 300);
}

export default {
  doAfterDelay,
  setActiveClass,
  moveAfterDelay,
  selectAndDelayMove
};
