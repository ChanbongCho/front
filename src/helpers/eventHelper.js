import Vue from "vue";
import ROSLIB from "roslib";
import { PUB } from "@/helpers/apiHelper";
// import {replaceAll} from "@/helpers/commonHelper";

export const EVENTBUS = new Vue();
export const EVENTS = {
  RECEIVE: {
    CONNECTION: "receive-connection",
    BRIDGE_PROCESS: "receive-from-bridge-process",
    DATA: "receive-data",
    SET_VIEW: "receive-set-view",
    SET_AVATAR: "receive-set-avatar",
    INITIAL_SEARCH_RESULT: "receive-initial-search-result",
    VOICE_SEARCH_RESULT: "receive-voice-search-result",
    PREVENT_CLICK_STATUS: "prevent-click-status",
    BATTERY_STATUS: "receive-battery-status",
    CHARGING_STATUS: "receive-charging-status",
    FIRE_POPUP_STATUS: "receive-fire-popup-status"
  },
  SEND: {
    DONE: "send-done",
    STATUS: "send-status",
    SCREEN_INPUT: "send-screen-input",
    POWER_OFF: "send-power-off",
    CHARGING: "send-charging",
    INITIAL_SEARCH: "send-initial-search",
    FIVE_SECOND_LEFT: "send-five-second-left",
    EMERGENCY_CALL: "send-emergency-call",
    EMERGENCY_CANCEL: "send-emergency-cancel",
    VOICE_RETRY: "send-voice-recognition-retry",
    TIMEOUT: "send-timeout",
    AVATAR_FACE: "send-avatar-face",
    AVATAR_COSTUME: "send-avatar-costume",
    REQ_MORE_TIME: "send-req-more-time",
    SAVE_SETTINGS: "save-settings",
    FIRE_POPUP_OFF: "send-fire-popup-off"
  },
  DIALOG: {
    CHANGE_DIALOG_STATUS: "change-dialog-status"
  },
  MAIN: {
    COUNTING: {
      RESTART: "restart-counting",
      RESET: "reset-counting",
      START: "start-counting",
      STOP: "remove-counting",
      PAUSE: "pause-counting",
      RESUME: "remove-counting"
    }
  },
  TEST: {
    CHANGE_PREVENT_CLICK_STATUS: "request-prevent-click-status",
    TO_BRIDGE: "request-to-bridge",
    SET_VIEW: "request-set-view",
    SET_AVATAR: "request-set-avatar",
    INITIAL_SEARCH_RESULT: "request-initial-search-result",
    VOICE_SEARCH_RESULT: "request-voice-search-result"
  }
};

let appVM = {};
let baseContentVM = {};

/* =================================
 *    전역 = 브릿지에서 수신후 처리 이벤트
 *    최상위라서 새로고침 하지 않으면 바뀌지 않기에 따로 이벤트 해제 없음
 * ================================== */
export function prepareAppEvents(context) {
  appVM = context;
  // 기본 이벤트 등록
  // 수신
  regReceiveConnection();
  regReceiveFromBridgeProcess();
  regReceiveFirePopupChange();
  regReceiveSetView();
  regReceiveSetAvatar();
  // 발신
  regSendStatusOnRouter();
  regSendStatus();
  regSendTimeout();
  regSendScreenInput();
  regSendEmergencyCall();
  regSendEmergencyCancel();
  regSendReqMoreTime();
  regSendFiveSecondsLeft();
  regSendAvatarFace();
  regSendAvatarCostume();
  regSendFirePopupOff();
  // 프론트 이벤트
  regCounting();
}

// 브릿지 서버 접속되었을 때 브라우저 새로고침
export function regReceiveConnection() {
  EVENTBUS.$on(EVENTS.RECEIVE.CONNECTION, () => {
    console.log("Connected");
    console.log("$forceUpdate");
    appVM.$forceUpdate();
    // window.location.reload();
  });
}

/*
 * 공통
 * 브릿지에서 수신한 신호 분기 처리
 * */
export function regReceiveFromBridgeProcess() {
  EVENTBUS.$on(EVENTS.RECEIVE.BRIDGE_PROCESS, message => {
    console.group("FromBridge 수신");
    const msgArray = message.split("|");
    const type = msgArray[0];
    const command = msgArray[1];
    let status;
    console.table({ 종류: type, 내용: command });
    console.groupEnd();
    switch (type) {
      case "data":
        EVENTBUS.$emit(EVENTS.RECEIVE.DATA, JSON.parse(command));
        break;
      case "reponse":
        console.log("브릿지에서 프론트로 응답 :", message);
        break;
      case "fire":
        // 화재감지 팝업 켜기만 수신
        status =
          command === "popup_on" ? true : command === "popup_off" ? false : "";
        EVENTBUS.$emit(EVENTS.RECEIVE.FIRE_POPUP_STATUS, status);
        break;
      case "initial":
        // 초성검색 결과 수신
        EVENTBUS.$emit(EVENTS.RECEIVE.INITIAL_SEARCH_RESULT, command);
        break;
      case "voice":
        // 음성검색 결과 수신
        EVENTBUS.$emit(EVENTS.RECEIVE.VOICE_SEARCH_RESULT, command);
        break;
      case "emergency":
        // 비상호출 창 닫기 수신
        status = command === "close";
        console.log("close qkedma", !status);
        EVENTBUS.$emit(EVENTS.DIALOG.CHANGE_DIALOG_STATUS, {
          show: !status
        });
        break;
      case "prevent":
        // 화면 클릭 막기 켜기 끄기 수신
        status = command === "on" ? true : command === "off" ? false : "";
        EVENTBUS.$on(EVENTS.RECEIVE.PREVENT_CLICK_STATUS, status);
        break;
      default:
    }
  });
}

/*
 * 공통
 * 화면 전환할 페이지 이름 수신
 * */
export function regReceiveSetView() {
  EVENTBUS.$on(EVENTS.RECEIVE.SET_VIEW, message => {
    const res = message.data.split("|");
    if (res.length === 2) {
      appVM.$router.push({
        name: res[0],
        params: { name: res[1] }
      });
    } else if (res.length === 1) {
      appVM.$router.push({ name: res[0] });
    }
  });
}

/*
 * 공통
 * 아바타 표정 변경 수신
 * */
export function regReceiveSetAvatar() {
  EVENTBUS.$on(EVENTS.RECEIVE.SET_AVATAR, message => {
    const res = message.data.split("|");
    const page = res[0];
    const faceType = res[1];
    console.log("page, face ", page, faceType);
    // appVM.$router.push({name: page});
    switch (page) {
      case "Monitor Page":
        appVM.$DB.avatar.monitor = faceType;
        break;
      // case "Patrol":
      //     appVM.$DB.avatar.patrol = faceType;
      //     break;
      case "Shot Avatar":
        appVM.$DB.avatar.picture = faceType;
        break;
    }
  });
}

// App.vue
export function regSendStatusOnRouter() {
  appVM.$router.beforeEach((to, from, next) => {
    console.group("Router BeforeEach 위치 판단 시작");
    offCountingEvents();
    regCounting();
    console.table({ from: from.path, to: to.path });
    EVENTBUS.$emit(EVENTS.SEND.STATUS, {
      pageName: to.name,
      pageParameter: to.params.name ? to.params.name : ""
    });
    cleatAllInterval();

    if (to.path.includes("main")) {
      console.log("Main 하위 경로라서 카운팅 시작", appVM.intervalId);
      EVENTBUS.$emit(EVENTS.MAIN.COUNTING.START);
    } else {
      console.log("Main 바깥 경로라서 카운팅 안함", appVM.intervalId);
    }
    // 경로 이동 후 화면 클릭해서 타임아웃 연장 막은 거 풀기
    appVM.$DB.stopForPageMove = false;
    appVM.$DB.stopForPopup = false;
    console.groupEnd();
    next();
  });
}

function offCountingEvents() {
  console.log("카운딩 이벤트 모두 제거");
  EVENTBUS.$off(EVENTS.MAIN.COUNTING.START, start);
  EVENTBUS.$off(EVENTS.MAIN.COUNTING.STOP, stop);
  EVENTBUS.$off(EVENTS.MAIN.COUNTING.RESTART, restart);
}

function regCounting() {
  console.log("카운딩 이벤트 모두 등록");
  EVENTBUS.$on(EVENTS.MAIN.COUNTING.START, start);
  EVENTBUS.$on(EVENTS.MAIN.COUNTING.STOP, stop);
  EVENTBUS.$on(EVENTS.MAIN.COUNTING.RESTART, restart);
}

function start() {
  appVM.leftTime = appVM.$DB.setting.timeoutSeconds;
  // appVM.leftTime = 200000;
  console.group(EVENTS.MAIN.COUNTING.START + "이벤트");
  console.log("timeoutSeconds :", appVM.$DB.setting.timeoutSeconds);
  appVM.intervalId = setInterval(() => {
    if (appVM.leftTime === 0) {
      console.log("time is ", 0);
      EVENTBUS.$emit(EVENTS.MAIN.COUNTING.STOP);
      EVENTBUS.$emit(EVENTS.SEND.TIMEOUT);
      appVM.$router.push("/monitor");
    } else {
      appVM.leftTime -= 1;
      console.log("time: ", appVM.leftTime);
    }
  }, 1000);
  console.groupEnd();
}

function stop() {
  console.group(EVENTS.MAIN.COUNTING.STOP + "이벤트");
  cleatAllInterval();
  console.groupEnd();
}

function restart() {
  console.group(EVENTS.MAIN.COUNTING.RESTART + "이벤트");
  cleatAllInterval();
  EVENTBUS.$emit(EVENTS.MAIN.COUNTING.START);
  console.groupEnd();
}

export function cleatAllInterval() {
  while (appVM.intervalId > 0) {
    // console.log('ids', appVM.intervalId);
    clearInterval(appVM.intervalId);
    appVM.intervalId -= 1;
  }
}

export function regReceiveFirePopupChange() {
  EVENTBUS.$on(EVENTS.RECEIVE.FIRE_POPUP_STATUS, status => {
    console.log(EVENTS.RECEIVE.FIRE_POPUP_STATUS + "이벤트 :", status);
    if (status) {
      EVENTBUS.$emit(EVENTS.DIALOG.CHANGE_DIALOG_STATUS, {
        modalType: "generalWarning",
        modalData: {
          isSos: true,
          title: "화재가 감지되었습니다.",
          closeButtonText: "확인"
        },
        show: status
      });
    } else {
      EVENTBUS.$emit(EVENTS.DIALOG.CHANGE_DIALOG_STATUS, { show: status });
    }
  });
}

/* =================================
 *      전역  브릿지로 발송 요청 이벤트
 * ================================== */

//
// 진료과 의료진 안내
// sendStatus('Medical Department List');
// 소개/의료진/위치:소아청소년과
// sendStatus('Medical Department detail', 'Pediatrics and Adolescent medicine');
// 표정: 미소
// sendStatus('Avatar Face', 'Smile');
export function regSendStatus() {
  EVENTBUS.$on(EVENTS.SEND.STATUS, data => {
    let location = new ROSLIB.Message({
      data: data.pageParameter
        ? `${data.pageName}|${data.pageParameter}`
        : `${data.pageName}`
    });
    PUB.status.publish(location);
  });
}

// 대기 화면에서 화면 클릭 시 input 신호 발송
export function regSendScreenInput() {
  EVENTBUS.$on(EVENTS.SEND.SCREEN_INPUT, () => {
    console.log(`${EVENTS.SEND.SCREEN_INPUT} 이벤트`);
    let msg = new ROSLIB.Message({
      data: "input"
    });
    PUB.toBridge.publish(msg);
    // console.log("screen input msg: ", msg.data);
  });
}

// 응급호출 신호 발송
export function regSendEmergencyCall() {
  EVENTBUS.$on(EVENTS.SEND.EMERGENCY_CALL, () => {
    let msg = new ROSLIB.Message({
      data: "emergency|call"
    });
    PUB.toBridge.publish(msg);
    console.log("emergency call msg: ", msg.data);
  });
}

// 응급호출 취소 신호 발송
export function regSendEmergencyCancel() {
  EVENTBUS.$on(EVENTS.SEND.EMERGENCY_CANCEL, () => {
    let msg = new ROSLIB.Message({
      data: "emergency|cancel"
    });
    PUB.toBridge.publish(msg);
    console.log("emergency cancel msg: ", msg.data);
  });
}

// 15초 간 입력 없음, 음성 입력 없음 신호 발송
export function regSendTimeout() {
  EVENTBUS.$on(EVENTS.SEND.TIMEOUT, () => {
    let msg = new ROSLIB.Message({
      data: "timeout"
    });
    PUB.toBridge.publish(msg);
    console.log("no input msg: ", msg.data);
  });
}

// 사진찍기에서 선택된 아바타 표정값 전송
export function regSendAvatarFace() {
  EVENTBUS.$on(EVENTS.SEND.AVATAR_FACE, picture => {
    let msg = new ROSLIB.Message({
      data: `avatar|${picture}`
    });
    PUB.toBridge.publish(msg);
    console.log("ava face msg: ", msg.data);
  });
}

// 사진찍기에서 선택된 아바타 의상값 전송
export function regSendAvatarCostume() {
  EVENTBUS.$on(EVENTS.SEND.AVATAR_COSTUME, costume => {
    let msg = new ROSLIB.Message({
      data: `dress|${costume}`
    });
    PUB.toBridge.publish(msg);
    console.log("ava costume msg: ", msg.data);
  });
}

// 사진찍기 5초 남음 신호 발송
export function regSendFiveSecondsLeft() {
  EVENTBUS.$on(EVENTS.SEND.FIVE_SECOND_LEFT, () => {
    let msg = new ROSLIB.Message({
      data: "picture|5s"
    });
    PUB.toBridge.publish(msg);
    console.log("5s left msg: ", msg.data);
  });
}

// 사진찍기 15초 지연 요청 신호 발송
export function regSendReqMoreTime() {
  EVENTBUS.$on(EVENTS.SEND.REQ_MORE_TIME, () => {
    let msg = new ROSLIB.Message({
      data: "picture|touch"
    });
    PUB.toBridge.publish(msg);
    console.log("more time msg: ", msg.data);
  });
}

// 화재감지 팝업 닫기 이벤트
export function regSendFirePopupOff() {
  EVENTBUS.$on(EVENTS.SEND.FIRE_POPUP_OFF, () => {
    let msg = new ROSLIB.Message({
      data: "fire|popup_off"
    });
    PUB.toBridge.publish(msg);
    console.log("more time msg: ", msg.data);
  });
}

/* =================================
 *      지역(특정 페이지 종속) 이벤트
 * ================================== */

/*
 * BT 부팅화면
 */
export function prepareBootingEvents() {
  regSendDone();
  regReceiveData();
}
export function disableBootingEvents() {
  EVENTBUS.$off(EVENTS.SEND.DONE, sendDone)
  EVENTBUS.$off(EVENTS.RECEIVE.DATA, receiveData);
}
// BT-0
// 브릿지에서 data 를 받으면 'done' 신호 발송 후 화면 자체 전환
export function regSendDone() {
  EVENTBUS.$on(EVENTS.SEND.DONE, sendDone)
}
function sendDone(){
  // console.log(`${EVENTS.SEND.DONE} 이벤트`);
  let msg = new ROSLIB.Message({
    data: "done"
  });
  PUB.toBridge.publish(msg);
  // console.log("data done msg: ", msg.data);
  appVM.$router.push({ path: "/boot/password" });
}
// BT-0
// 브릿지에서 보내주는 데이터 수신 후
// 데이터 반복 송출을 중지하라고 done 신호를 발송
export function regReceiveData() {
  EVENTBUS.$on(EVENTS.RECEIVE.DATA, receiveData);
}
function receiveData(res){
  // console.log("data", res);
  console.log(EVENTS.RECEIVE.DATA, "이벤트 ", res.timeoutSeconds);
  appVM.$DB.password = res.password;
  appVM.$DB.setting = res.setting;
  appVM.$DB.department = res.department;
  appVM.$DB.isDataSet = true;
  EVENTBUS.$emit(EVENTS.SEND.DONE);
}

/*
 * AD 관리자 설정
 * */
let adminVM, batteryVM;

export function disablePowerOffEvent(){
  EVENTBUS.$off(EVENTS.SEND.POWER_OFF, sendPowerOff);
}

// AD_B "POWER OFF" 버튼 클릭 시 /dt/front_end/front_end_to_bridge "power|off" publish
export function regSendPowerOff() {
  console.log('regSendPowerOff')
  EVENTBUS.$on(EVENTS.SEND.POWER_OFF, sendPowerOff);
}
function sendPowerOff(){
  let msg = new ROSLIB.Message({
    data: "power|off"
  });
  PUB.toBridge.publish(msg);
  console.log("msg send: ", msg.data);
}

export function disableSaveSettingsEvents(){
  EVENTBUS.$off(EVENTS.SEND.SAVE_SETTINGS, saveSettings);
}

// "확인" 버튼 클릭 시 설정 정보 publish
export function regSaveSettings(context) {
  adminVM = context;
  EVENTBUS.$on(EVENTS.SEND.SAVE_SETTINGS, saveSettings);
}
function saveSettings(){
  // console.log(`${EVENTS.SEND.SAVE_SETTINGS} 이벤트`);
  const settingStr = JSON.stringify({ setting: adminVM.$DB.setting });
  let msg = new ROSLIB.Message({
    data: `data|${settingStr}`
  });
  PUB.toBridge.publish(msg);
  // console.log("setting msg: ", msg.data);
}

// AD_B 배터리
export function prepareBatteryEvents(context) {
  batteryVM = context;
  regReceiveBatteryStatus();
  regReceiveChargingStatus();
  regSendCharging();
}

export function disableBatteryEvents(){
  EVENTBUS.$off(EVENTS.SEND.CHARGING, sendCharging);
  EVENTBUS.$off(EVENTS.RECEIVE.BATTERY_STATUS, receiveBatteryStatus);
  EVENTBUS.$off(EVENTS.RECEIVE.CHARGING_STATUS, receiveChargingStatus);
}

// AD_B "Docking" 버튼 클릭 시 /dt/front_end/front_end_to_bridge "charging" publish
export function regSendCharging() {
  EVENTBUS.$on(EVENTS.SEND.CHARGING, sendCharging);
}
function sendCharging(){
    // console.log(`${EVENTS.SEND.CHARGING} 이벤트`);
    let msg = new ROSLIB.Message({
      data: `charging`
    });
    PUB.toBridge.publish(msg);
    // console.log("charging msg: ", msg.data);
}

// AD_B 배터리 상태 32비트 정수 타입 수신
export function regReceiveBatteryStatus() {
  EVENTBUS.$on(EVENTS.RECEIVE.BATTERY_STATUS, receiveBatteryStatus);
}

function receiveBatteryStatus(message){
  // console.log(`${EVENTS.RECEIVE.BATTERY_STATUS} 이벤트`);
  batteryVM.battery = message.data;
  // console.log("msg: ", msg.data);
}

// AD_B 도킹/언도킹 여부 불리언 타입 수신
export function regReceiveChargingStatus() {
  EVENTBUS.$on(EVENTS.RECEIVE.CHARGING_STATUS, receiveChargingStatus);
}

function receiveChargingStatus(message){
// console.log(`${EVENTS.RECEIVE.CHARGING_STATUS} 이벤트`);
  batteryVM.charging = message.data;
  // console.log("msg: ", msg.data);
}


/*
 * 초성검색
 * */
let initialSearchVM;

export function prepareLocationSearchEvents(context) {
  initialSearchVM = context;
  regSendInitialInput();
  regReceiveInitialSearchResult();
}
export function disableLocationSearchEvents(){
  EVENTBUS.$off(EVENTS.SEND.INITIAL_SEARCH, sendInitialInput);
  EVENTBUS.$off(EVENTS.RECEIVE.INITIAL_SEARCH_RESULT, processInitialResult);
}
// 초성검색 입력값 발송
export function regSendInitialInput() {
  EVENTBUS.$on(EVENTS.SEND.INITIAL_SEARCH, sendInitialInput);
}
function sendInitialInput(input){
  // console.log(EVENTS.SEND.INITIAL_SEARCH + "이벤트");
  const charToUnicode = function(str) {
    if (!str) return false;
    let unicodeInRedix10 = "";
    for (let i = 0, j = str.length; i < j; i++) {
      // unicode += '\\' + str[i].charCodeAt(0).toString(16);
      // 통합측이 python2 환경이라. 10진수 그대로 보내기로 함.
      unicodeInRedix10 += "\\" + str[i].charCodeAt(0);
    }
    return unicodeInRedix10;
  };
  // const initialUnicode = escape(replaceAll(input, "\\", "%"));
  const initialUnicode = charToUnicode(input);
  let msg = new ROSLIB.Message({
    data: `initial|${initialUnicode}`
  });
  PUB.toBridge.publish(msg);
  console.log("initial msg: ", msg.data);
}

// 초성검색 결과 수신
export function regReceiveInitialSearchResult() {
  EVENTBUS.$on(EVENTS.RECEIVE.INITIAL_SEARCH_RESULT, processInitialResult);
}

export function processInitialResult(res) {
  const data = JSON.parse(res)["result"];
  console.log("data: ", data);
  console.log("is Array: ", Array.isArray(data));
  console.log("1st result: ", data[0]);
  EVENTBUS.$emit(EVENTS.MAIN.COUNTING.RESTART);
  if (Array.isArray(data)) {
    initialSearchVM.hasNoInitialResult = false;
    appVM.$DB.initialResultArray = data;
  } else {
    initialSearchVM.hasNoInitialResult = true;
    appVM.$DB.initialResultArray = [];
  }
}

/*
 * 음성인식
 * */
let voiceSearchVM;

export function prepareVoiceSearchEvents(context) {
  voiceSearchVM = context;
  regReceiveVoiceSearchResult();
  regSendVoiceRecognitionRetry();
}

export function disableVoiceSearchEvents() {
  EVENTBUS.$off(EVENTS.RECEIVE.VOICE_SEARCH_RESULT, processResult);
  EVENTBUS.$off(EVENTS.RECEIVE.VOICE_RETRY, retry);
}

// 음성인식 결과 수신
export function regReceiveVoiceSearchResult() {
  EVENTBUS.$on(EVENTS.RECEIVE.VOICE_SEARCH_RESULT, processResult);
}

// 음성인식 다시 묻기 신호 발송
export function regSendVoiceRecognitionRetry() {
  EVENTBUS.$on(EVENTS.SEND.VOICE_RETRY, retry);
}

function processResult(res) {
  console.group(EVENTS.RECEIVE.VOICE_SEARCH_RESULT + "이벤트");
  // console.log("voice data", data);
  const data = JSON.parse(res);
  console.log(data);
  console.log(data.result);
  console.log(data.keyword);
  voiceSearchVM.hasResult = true;
  voiceSearchVM.voiceKeywordArray = data.keyword;
  voiceSearchVM.voiceQuestionStr = data.result;

  voiceSearchVM.$DB.stopForPageMove = false;
  EVENTBUS.$emit(EVENTS.MAIN.COUNTING.START);
  console.groupEnd();
}

function retry() {
  voiceSearchVM.retryBtnClicked = true;
  voiceSearchVM.hasResult = false;
  voiceSearchVM.voiceKeywordArray = [];
  voiceSearchVM.voiceQuestionStr = "";
  let msg = new ROSLIB.Message({
    data: "voice|retry"
  });
  PUB.toBridge.publish(msg);
  console.log("voice retry msg: ", msg.data);
}

// BaseContentsContainer
// 베이스 컨테이너는 새로고침 하지 않으면 바뀌지 않기에 따로 이벤트 해제 없음
export function prepareBaseContentEvents(context) {
  baseContentVM = context;
  regChangePreventClickStatus();
  regRequestToBridge();
  regRequestToBridgeSetView();
  regRequestToBridgeChosungResult();
  regRequestToBridgeVoiceResult();
}

function regChangePreventClickStatus() {
  EVENTBUS.$on(EVENTS.TEST.CHANGE_PREVENT_CLICK_STATUS, prevent => {
    if (prevent) {
      console.log(baseContentVM.$refs.temp);
      baseContentVM.$refs.temp.classList.add("freeze");
    } else {
      baseContentVM.$refs.temp.classList.remove("freeze");
    }
  });
}

function regRequestToBridge() {
  EVENTBUS.$on(EVENTS.TEST.TO_BRIDGE, message => {
    let msg = new ROSLIB.Message({
      data: message
    });
    PUB.toBridge.publish(msg);
    console.log("브릿지로: ", msg.data);
  });
}

function regRequestToBridgeSetView() {
  EVENTBUS.$on(EVENTS.TEST.SET_VIEW, () => {
    let msg = new ROSLIB.Message({
      data: baseContentVM.topicMsg
    });
    PUB.toBridge.publish(msg);
    console.log("브릿지로 페이지 이동 요청: ", msg.data);
  });
}

function regRequestToBridgeChosungResult() {
  EVENTBUS.$on(EVENTS.TEST.INITIAL_SEARCH_RESULT, () => {
    let msg = new ROSLIB.Message({
      data: `request|chosungres/${baseContentVM.chosungResult}`
    });
    PUB.toBridge.publish(msg);
    console.log("브릿지 서버로 초성검색 결과 발송 요청: ", msg.data);
  });
}

function regRequestToBridgeVoiceResult() {
  EVENTBUS.$on(EVENTS.TEST.VOICE_SEARCH_RESULT, () => {
    let msg = new ROSLIB.Message({
      data: `request|voiceres/${baseContentVM.voiceKeyword}/${baseContentVM.voiceResult}`
    });
    PUB.toBridge.publish(msg);
    console.log("브릿지 서버로 음성검색 결과 발송 요청: ", msg.data);
  });
}

/* =================================
 *                설정
 * ================================== */

// 새로고침 등 페이지 다시 접속할 때 이벤트가 쌓여서 여러번 실행되는 것을 방지
export function removeAllEvents() {
  EVENTBUS.$off();
}

export default {
  prepareEvents: prepareAppEvents,
  EVENTBUS,
  EVENTS
};
