# Header

헤더부

## Methods

<!-- @vuese:Header:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|소리 재생|-|
|incrementClickCount|클릭 카운트 늘리기|-|
|operateHiddenButton|히든 버튼 클릭 시 카운트 증가시키고, 5회 충족되면 관리자 첫화면으로 이동|-|
|historyBack|뒤로가기 누르면, 음성검색 페이지 제외하고는 브라우저 히스토리 뒷쪽으로 페이지 이동|-|
|home|홈버튼 누르면 메인화면으로 이동|-|
|close|닫기 누르면 모니터로 이동하면서 타임아웃 신호 브릿지로 발송|-|
|emergencyCall|'응급호출' 버튼 선택 시 신호 발송 & 팝업 오픈|-|
|addAndRemoveActive|액티브 클래스 추가했다가 제거하기|-|

<!-- @vuese:Header:methods:end -->


