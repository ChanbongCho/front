# DepartmentList

진료과 목록 페이지

## Methods

<!-- @vuese:DepartmentList:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|generateData|저장소 에서 진료과 데이터를 뽑아 데이터 배열을 만든다|-|
|playSound|효과음 재생|sound 재생할 사운드|
|moveToChildPage|진료과 선택 시 하위 라우트로 지연 이동 |dep 선택된 진료과|

<!-- @vuese:DepartmentList:methods:end -->


