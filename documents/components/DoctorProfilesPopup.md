# DoctorProfilesPopup

의료진 상세 팝업

## Props

<!-- @vuese:DoctorProfilesPopup:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|show|-|—|`false`|-|
|modalType|-|—|`false`|-|
|modalData|-|—|`false`|-|

<!-- @vuese:DoctorProfilesPopup:props:end -->


## Events

<!-- @vuese:DoctorProfilesPopup:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|close|-|-|

<!-- @vuese:DoctorProfilesPopup:events:end -->


## Methods

<!-- @vuese:DoctorProfilesPopup:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|소리 재생|sound 소리명|
|open|창 열기. 기본 데이터를 설정하고 20초 타임아웃 후 모니터페이지 이동 설정|-|
|confirm|확인 누를 시 부모로 close 이벤트 발송 및 컨펌 액션 실행|-|
|close|창 닫기|-|
|showNextPage|다음 페이지 보여주기|-|
|showPreviousPage|이전 페이지 보여주기|-|
|moveToNextPage|좌우 버튼 클릭 시 active 클래스 적용하고 0.3초 후 페이지 이동|-|
|moveToPreviousPage|좌우 버튼 클릭 시 active 클래스 적용하고 0.3초 후 페이지 이동|-|

<!-- @vuese:DoctorProfilesPopup:methods:end -->


