# SelectFace

아바타 표정 선택 페이지

## Methods

<!-- @vuese:SelectFace:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|효과음 재생|sound 재생할 사운드|
|confirm|확인 버튼 클릭 시 active 클래스 적용 후 지연 이동|e 클릭 이벤트|
|goToNextStep|선택된 표정값이 빈값이 아니면 서버에 표정값 전송하고 의상 선택 페이지로 이동|-|
|addAndRemoveActive|active 클래스 적용/제거|refName 선택한 표정의 refName|
|setFace|표정 선택|refName, str 선택한 표정의 refName 과 표정값|

<!-- @vuese:SelectFace:methods:end -->


