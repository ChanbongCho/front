# AdminAuthentication

부팅 암호 입력 페이지

## Methods

<!-- @vuese:AdminAuthentication:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|getCursorPosition|현재 인풋 커서 위치를 가져와 설정한다.|-|
|endInput|암호 입력 인풋폼에 포커스를 적용한다|-|
|movePositionWith|주어진 양(양/음수)만큼 기존 커서 위치를 조정한다.|-|
|addSelectedInitialCha|각 키별 입력 처리|-|
|resetInputValue|입력값 초기화|-|
|checkPassword|암호 검증|-|

<!-- @vuese:AdminAuthentication:methods:end -->


