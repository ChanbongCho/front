# PatrolScheduleControl

순찰 스케쥴 설정 페이지

## Methods

<!-- @vuese:PatrolScheduleControl:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|setMaxMinTimeIn29Format|순찰시간으로부터 값을 가져와서 시간입력 최대 최소값을 29시간 형식으로 생성한다.|-|
|setPatrolInfoIn29Format|24시간 패트롤 시간과 문자열 플래그를 받아서 29시간 플래그 타입으로 변환된 값의 배열 생성|-|
|copyTimeObj|주어진 값으로 새로운 시간 객체를 반환|주어진 시간 객체 대체할 시간 대체할 분|
|minTimeObj|최소시간을 가진 시간 객체를 반환|-|
|maxTimeObj|최대시간을 가진 시간 객체를 반환|-|
|midTimeObj|중간시간을 가진 시간 객체를 반환. 객체가 3개일 때만 고려한다.|-|
|setProperDefaultTime|현재 시간에 적절한 초기 시간을 찾아서 할당한다|-|
|loadPatrol|주어진 슬롯에 맞는 스케쥴을 로드하며 현재 필요한 값을 설정한다|num 슬롯 번호|
|checkRulesAndSetNotice|주어진 시/분으로 규칙을 점검후 메시지를 보여주며 오류 값을 반환한다.|currentHour 현재 시 currentMinute 현재 분 perform 슬롯 활성상태|
|raiseHour|현재 시 늘이기|-|
|reduceHour|현재 시 줄이기|-|
|raiseMinute|현재 분 늘이기|-|
|reduceMinute|현재 분 줄이기|-|
|togglePeriod|AM/PM 변경 버튼 클릭 시, +/- 12시간 계산|-|
|isOutOfLimit|error 2. 순찰 스케줄이 경비 스케줄 시간 범위를 벗어났는지 여부|timeObj 시간 객체|
|isOverlap|error 3. 순찰 스케줄간 간격이 겹치는 지 여부. 분 차이가 60 보다 작으면 true|timeObj 시간 객체 cidx 계산 제외할 현재시간 객체|
|setNotice|현재 알림 종류를 설정|type 알림 종류|
|addAndActive|활성 클래스 추가 후 제거|el 이벤트 타겟 callback 콜백함수|
|close|취소 버튼 팝업 보이기|e 이벤트 객체|
|hasError|활성화된 순찰 시간에 오류가 있는지 여부|-|
|saveAndMoveToMonitorPage|변경한 값을 저장|-|
|popConfirm|확인 팝업 띄우기|desc 팝업 본문 내용 btnTxt 팝업 확인 버튼 내용|

<!-- @vuese:PatrolScheduleControl:methods:end -->


## Computed

<!-- @vuese:PatrolScheduleControl:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|currentHour|-|현재 시|No|
|currentMinute|-|현재 분|No|
|periodName|-|시간 AM/PM|No|
|nightTimeGapInHour|-|야간 스케쥴 시작부터 끝까지 시간의 차|No|
|availablePatrolCount|-|가능한 순찰 슬롯 개수|No|
|currentWillPerform|-|현재 스케쥴 수행 여부|No|
|twoDigitHour|-|현재 시의 문자열. 29시간 값을 24시간 값으로 변환.|No|
|twoDigitMinutes|-|현재 분의 문자열|No|
|noticeText|-|현재 안내 타입에 따라 안내 문자열 출력|No|

<!-- @vuese:PatrolScheduleControl:computed:end -->


## Data

<!-- @vuese:PatrolScheduleControl:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|nightScheduleLimit|`Object`|경비 스케줄 최대/최소값. 18:00 ~ 05:59 범위 내에서 설정 가능|-|

<!-- @vuese:PatrolScheduleControl:data:end -->


