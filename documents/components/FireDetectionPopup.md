# FireDetectionPopup

화재감지 팝업

## Props

<!-- @vuese:FireDetectionPopup:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|show|-|—|`false`|-|
|modalType|-|—|`false`|-|
|modalData|-|—|`false`|-|

<!-- @vuese:FireDetectionPopup:props:end -->


## Events

<!-- @vuese:FireDetectionPopup:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|close|-|-|

<!-- @vuese:FireDetectionPopup:events:end -->


## Methods

<!-- @vuese:FireDetectionPopup:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|open|창 열기. 기본 데이터를 설정하고 20초 타임아웃 후 모니터페이지 이동 설정|-|
|confirm|확인 누를 시 부모로 close 이벤트 발송 및 컨펌 액션 실행|-|
|close|창 닫기|-|

<!-- @vuese:FireDetectionPopup:methods:end -->


