# AdminHeader

설정 페이지 상단 헤더부. 설정 페이지에서만 보인다.

## Methods

<!-- @vuese:AdminHeader:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|소리재생|sound 소리명|
|addAndRemoveActive|active 클래스 추가 및 제거|el 이벤트 타겟 callback 콜백함수|
|close|어드민 첫화면으로 이동|e 이벤트 객체|

<!-- @vuese:AdminHeader:methods:end -->


