# EmergencyCallPopup

응급호출 팝업

## Props

<!-- @vuese:EmergencyCallPopup:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|show|-|—|`false`|-|
|modalType|-|—|`false`|-|
|modalData|-|—|`false`|-|

<!-- @vuese:EmergencyCallPopup:props:end -->


## Events

<!-- @vuese:EmergencyCallPopup:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|close|-|-|

<!-- @vuese:EmergencyCallPopup:events:end -->


## Methods

<!-- @vuese:EmergencyCallPopup:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|소리 재생|sound 소리명|
|open|창 열기. 기본 데이터를 설정하고 20초 타임아웃 후 모니터페이지 이동 설정|-|
|confirm|확인 누를 시 부모로 close 이벤트 발송 및 컨펌 액션 실행|-|
|close|창 닫기|-|
|disableBtn|5초 후 버튼 비활성화 타임아웃 설정|-|

<!-- @vuese:EmergencyCallPopup:methods:end -->


