# SoundSetting

볼륨 크기 설정 페이지

## Methods

<!-- @vuese:SoundSetting:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|addAndActive|활성 클래스 추가 후 제거|el 이벤트 타겟 callback 콜백함수|
|close|취소 버튼 팝업 보이기|e 이벤트 객체|
|saveValue|저장 후 확인 팝업 보이기|e 이벤트 객체|
|popConfirm|확인 팝업 띄우기|desc 팝업 본문 내용 btnTxt 팝업 확인 버튼 내용|

<!-- @vuese:SoundSetting:methods:end -->


