# BatterySetting

배터리 설정

## Methods

<!-- @vuese:BatterySetting:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|sendCharging|충전 신호를 브릿지로 발송|e 이벤트 객체|
|addAndRemoveActive|활성 클래스 추가 후 제거|el 이벤트 타겟 callback 콜백함수|

<!-- @vuese:BatterySetting:methods:end -->


