# ConfirmPopup

확인 팝업

## Props

<!-- @vuese:ConfirmPopup:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|show|-|—|`false`|-|
|modalType|-|—|`false`|-|
|modalData|-|—|`false`|-|

<!-- @vuese:ConfirmPopup:props:end -->


## Events

<!-- @vuese:ConfirmPopup:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|close|-|-|

<!-- @vuese:ConfirmPopup:events:end -->


## Methods

<!-- @vuese:ConfirmPopup:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|소리 재생|sound 소리명|
|open|창 열기|-|
|confirm|확인 누를 시 부모로 close 이벤트 발송 및 컨펌 액션 실행|-|
|close|창 닫기|-|

<!-- @vuese:ConfirmPopup:methods:end -->


