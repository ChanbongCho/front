# LocationSearchKeypad

초성검색 페이지 키패드

## Props

<!-- @vuese:LocationSearchKeypad:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|searchText|검색어|`String`|`false`|-|
|cursorPosition|커서 위치|`Number`|`false`|-|

<!-- @vuese:LocationSearchKeypad:props:end -->


## Events

<!-- @vuese:LocationSearchKeypad:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|end|-|-|
|do-search|-|-|

<!-- @vuese:LocationSearchKeypad:events:end -->


## Methods

<!-- @vuese:LocationSearchKeypad:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|applyActiveClass|선택된 키에 active 클래스를 적용한다|key 선택된 키|
|removeWithInterval|active 클래스를 제거한다.|-|
|playSound|효과음 재생|sound 재생할 사운드|
|addSelectedInitialCha|선택된 키에 따라 초성 클릭 시 해당 초성을 입력값에 추가 / backspace 클릭 시 문자 삭제 / ENTER 클릭 시 검색을 진행한다|key 선택된 키|
|doSearch|입력값에 따라 검색 이벤트 emit 하거나, 입력값이 빈값일 경우 searchStatus 업데이트 한다|key 선택된 키|
|deleteOneCharacter|입력값에서 커서 앞의 문자 하나를 삭제한다|key 선택된 키|
|movePositionWith|현재 커서 위치를 주어진 수 만큼 조정해서 이동한다.|amount 양수/음수|

<!-- @vuese:LocationSearchKeypad:methods:end -->


