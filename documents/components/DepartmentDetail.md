# DepartmentDetail

진료과 상세 페이지

## Methods

<!-- @vuese:DepartmentDetail:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|효과음 재생|sound 재생할 사운드|
|moveToLocationPage|층별 안내 페이지로 이동하여 해당 진료과의 위치를 지도 상에 표시한다|-|
|modalData|의료진 상세 팝업 오픈 시 전달할 모달 데이터 반환|-|
|showModal|의료진 상세 팝업 오픈|profileOrd 해당 의료진의 ord 넘버|
|showNextPage|다음 의료진 목록을 표시한다|-|
|showPreviousPage|이전 의료진 목록을 표시한다|-|
|moveToNextPage|오른쪽 버튼 클릭 시 active 클래스 적용하고, 다음 의료진 목록으로 지연 이동|-|
|moveToPreviousPage|왼쪽 버튼 클릭 시 active 클래스 적용하고, 이전 의료진 목록으로 지연 이동|-|

<!-- @vuese:DepartmentDetail:methods:end -->


## Computed

<!-- @vuese:DepartmentDetail:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|departmentList|-|저장소 에서 읽어온 의료진 리스트 데이터|No|
|currentDepartment|-|의료진 리스트 데이터에서 추출한 해당 진료과 데이터|No|
|doctors|-|해당 진료과의 의료진 데이터|No|
|extractShowPart|-|화면에 표시할 의료진 목록 추출|No|
|isNextAvailable|-|다음 목록이 있는지 여부|No|
|isPreviousAvailable|-|이전 목록이 있는지 여부|No|
|maxPageIndex|-|전체 페이지 수|No|

<!-- @vuese:DepartmentDetail:computed:end -->


## Data

<!-- @vuese:DepartmentDetail:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|currentPageIndex|`Number`|현재 의료진 목록 페이지 인덱스|1|
|unit|`Number`|한 페이지에 보여줄 의료진 목록 수|4|
|selectedProfileNumber|`Number`|선택된 의료진 넘버|0|

<!-- @vuese:DepartmentDetail:data:end -->


