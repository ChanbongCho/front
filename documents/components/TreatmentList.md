# TreatmentList

진료안내 외래/응급진료 페이지

## Methods

<!-- @vuese:TreatmentList:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|효과음 재생|sound 재생할 사운드|
|moveToChildPage|하위 라우트 페이지로 이동|path 하위 라우트 경로, status 박스 선택 여부|

<!-- @vuese:TreatmentList:methods:end -->


