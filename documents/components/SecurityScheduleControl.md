# SecurityScheduleControl

야간 스케쥴 설정 페이지

## Methods

<!-- @vuese:SecurityScheduleControl:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|setDefaultSchedule|전역 저장소에 저장된 기존 스케줄을 가져와 기본값으로 세팅|-|
|setActiveClass|버튼 클릭 시, 해당 unit 에 active 클래스 적용|-|
|playSound|소리 재생|sound 소리명|
|checkHourIsOutOfLimit|주어진 시간이 최대/최소 범위를 벗어났는지 여부 반환|currentHour 현재 시간|
|checkMinuteIsOurOfLimit|주어진 분이 최대 범위를 벗어났는지 여부|startHour 시작 시간 endHour 끝 시간 currentMinute 현재 분|
|checkStarAndEndIS24Hour|시작과 끝 시간이 24시간이 되는 지 여부|startHour 시작 시간 endHour 끝 시간 startMinute 현재 분 endMinute 현재 분|
|checkStarAndEndIsReversed|끝시간이 시작시간보다 앞섰는지 여부|startHour 시작 시간 endHour 끝 시간 startMinute 현재 분 endMinute 현재 분|
|setNotice|메시지 종류 값을 받아서 해당되는 메시지를 하단에 출력한다.|type 경고 메시지 종류 값|
|raiseHour|시간 늘리기|hour 시간|
|reduceHour|시간 줄이기|hour 시간|
|raiseMinute|분 늘이기|minute 분|
|reduceMinute|분 줄이기|minute 분|
|holdChangedValue|29 형태의 현재 시간을 24시간 형태의 문자열로 변환 후 저장|-|
|togglePeriod|AM/PM 변경 버튼 클릭 시, +/- 12시간 계산|-|
|resetPatrolSchedule|경비스케줄을 설정을 변경할 경우, 순찰 스케줄 데이터 모두 삭제(디폴트 값 없음/수행여부도 체크되지 않음)|el 이벤트 타겟 callback 콜백함수|
|addAndActive|활성 클래스 추가 후 제거|el 이벤트 타겟 callback 콜백함수|
|close|취소 버튼 팝업 보이기|e 이벤트 객체|
|saveAndMoveToMonitorPage|저장 후 확인 팝업 보이기|e 이벤트 객체|
|popConfirm|확인 팝업 띄우기|desc 팝업 본문 내용 btnTxt 팝업 확인 버튼 내용|

<!-- @vuese:SecurityScheduleControl:methods:end -->


## Computed

<!-- @vuese:SecurityScheduleControl:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|startHourStr|-|시작 시간 문자열. startHour 29시간 숫자를 24시간 문자열로 변환.|No|
|startMinuteStr|-|시작 분 문자열. 내부에 숫자를 문자열로 변경.|No|
|endHourStr|-|끝 시간 문자열. endHour 29시간 숫자를 24시간 문자열로 변환.|No|
|endMinuteStr|-|끝 분 문자열. 내부에 숫자를 문자열로 변경.|No|
|startPeriodName|-|시작시간 AM/PM|No|
|endPeriodName|-|끝시간 AM/PM|No|

<!-- @vuese:SecurityScheduleControl:computed:end -->


## Data

<!-- @vuese:SecurityScheduleControl:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|nightScheduleLimit|`Object`|경비 스케줄 최대/최소값. 18:00 ~ 05:59 범위 내에서 설정 가능|-|

<!-- @vuese:SecurityScheduleControl:data:end -->


