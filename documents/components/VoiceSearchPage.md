# VoiceSearchPage

음성인식 페이지

## Methods

<!-- @vuese:VoiceSearchPage:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|효과음 재생|sound 재생할 사운드|
|addAndRemoveActive|active 클래스 적용/제거|refName 선택한 결과 라인의 refName|
|processResult|안내 페이지로 이동|result, refName 선택한 결과 라인, 결과 라인의 refName|
|retry|다시묻기 진행|-|

<!-- @vuese:VoiceSearchPage:methods:end -->


## Computed

<!-- @vuese:VoiceSearchPage:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|voiceResultList|-|서버에서 받은 음성인식 결과 배열로부터 화면에 표시할 결과 목록을 만든다. 결과 키워드가 진료과인 경우, 진료과 안내 정보 & 위치 안내 정보 바로가기 표시. 결과 키워드가 진료과 외 시설인 경우, 위치 안내 정보 바로가기 표시.|No|

<!-- @vuese:VoiceSearchPage:computed:end -->


