# LocationSearch

초성검색 페이지

## Methods

<!-- @vuese:LocationSearch:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|getCursorPosition|입력창 커서 위치 가져온다|e 클릭 이벤트|
|playSound|효과음 재생|sound 재생할 사운드|
|requestInitialSearch|서버에 초성검색 요청을 보낸다|-|
|doSearch|입력값이 비어 있지 않으면, 초성검색 요청한다|-|
|showKeypadToggle|검색 결과가 없을 경우 키패드를 보여주고, 검색 결과가 있을 경우, 검색 화면을 보여준다|-|
|endInput|입력창에 포커스를 설정한다|-|

<!-- @vuese:LocationSearch:methods:end -->


