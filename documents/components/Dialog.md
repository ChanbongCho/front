# Dialog

하위 다이얼로그를 포함하는 다이얼로그 부모 컴포넌트

## Methods

<!-- @vuese:Dialog:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|regChangeDialogStatus|EVENTS.DIALOG.CHANGE_DIALOG_STATUS 이벤트 등록|-|
|close|창 닫기|-|

<!-- @vuese:Dialog:methods:end -->


## Watch

<!-- @vuese:Dialog:watch:start -->
|Name|Description|Parameters|
|---|---|---|
|show|show 값이 true 면 모달 종류에 맞는 모달을 보이게 하고, false 면 하위 모달을 닫고 바탕 페이지 경로를 브릿지로 보낸다.|-|

<!-- @vuese:Dialog:watch:end -->


