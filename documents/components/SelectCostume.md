# SelectCostume

아바타 의상 선택 페이지

## Methods

<!-- @vuese:SelectCostume:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|효과음 재생|sound 재생할 사운드|
|confirm|확인 버튼 클릭 시 active 클래스 적용 후 지연 이동|e 클릭 이벤트|
|goToNextStep|선택된 의상값이 빈값이 아니면 서버에 값 전송하고 다음 페이지로 이동|-|
|addAndRemoveActive|active 클래스 적용/제거|refName 선택한 표정의 refName|
|setCostume|의상 선택|refName, str 선택한 의상의 refName 과 의상값|

<!-- @vuese:SelectCostume:methods:end -->


