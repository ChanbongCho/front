# LocationFloorList

층별 안내 페이지

## Methods

<!-- @vuese:LocationFloorList:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|findAndActivateLocation|특정 키워드로 해당 시설 위치 확인을 위해 진입한 경우, 해당 시설의 위치를 바로 표시한다 (예. 진료과 -> 위치 안내 / 음성인식 -> 위치안내 바로가기)|-|
|playSound|효과음 재생|sound 재생할 사운드|
|moveToInitialSearchPage|초성검색 페이지로 이동|-|
|onClickFloorName|층 이름 클릭 시, 해당 층의 지도 이미지와 정보를 보여준다|name 층 이름|
|onClickRoom|시설 클릭 시, 지도에서 해당 시설의 위치를 표시한다|num 시설의 번호|

<!-- @vuese:LocationFloorList:methods:end -->


## Data

<!-- @vuese:LocationFloorList:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|pointingPlace|`Object`|지도 상 위치 표시할 지점 (position 은 화면 상의 좌표)|-|

<!-- @vuese:LocationFloorList:data:end -->


