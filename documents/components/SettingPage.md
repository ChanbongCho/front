# SettingPage

하위 설정을 담는 부모 설정 페이지

## Methods

<!-- @vuese:SettingPage:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|소리 재생|sound 소리명|
|powerOff|파워 오프 신호 보내기|e 이벤트 객체|
|addAndRemoveActive|활성 클래스 추가 후 제거|el 이벤트 타겟 callback 콜백함수|
|moveTo|페이지 이동|e 이벤트 타겟 path 이동할 경오|

<!-- @vuese:SettingPage:methods:end -->


