# TemperatureSetting

이상체온 설정

## Methods

<!-- @vuese:TemperatureSetting:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|setDefaultTemp|전역 저장소에 저장된 기존 이상체온설정을 가져와 기본값으로 세팅|-|
|setActiveClass|버튼 클릭 시, 해당 unit 에 active 클래스 적용|-|
|playSound|소리 재생|sound 소리명|
|raiseTemp|온도 높이기|temp 온도|
|lowerTemp|온도 낮추기|temp 온도|
|toggleMode|이상/ 이하 모드 변경|-|
|addAndRemoveActive|활성 클래스 제거|el 이벤트 타겟 callback 콜백함수|
|addAndActive|활성 클래스 추가|el 이벤트 타겟 callback 콜백함수|
|close|취소 버튼 팝업 보이기|e 이벤트 객체|
|saveValue|저장 후 확인 팝업 보이기|e 이벤트 객체|
|popConfirm|확인 팝업 띄우기|desc 팝업 본문 내용 btnTxt 팝업 확인 버튼 내용|

<!-- @vuese:TemperatureSetting:methods:end -->


## Computed

<!-- @vuese:TemperatureSetting:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|guideText|-|현재 온도 안내 문자열|No|
|currentTempValue|-|저장을 위해 변환된 온도 범위|No|

<!-- @vuese:TemperatureSetting:computed:end -->


