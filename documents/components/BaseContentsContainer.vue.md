# BaseContentsContainer.vue

헤더와 컨텐츠 본문을 담는 베이스 컨테이너

## Methods

<!-- @vuese:BaseContentsContainer.vue:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|resetLeftTimeAndEndInput|화면 영역을 클릭하면, 남은 시간을 초기화 한다.|-|

<!-- @vuese:BaseContentsContainer.vue:methods:end -->


## Computed

<!-- @vuese:BaseContentsContainer.vue:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|isNoHeaderPage|-|헤더 출력하지 않음 여부|No|
|isAdmin|-|관리자 페이지인지 여부|No|

<!-- @vuese:BaseContentsContainer.vue:computed:end -->


