# MainList

메인 컨텐츠부, 초기 화면

## Methods

<!-- @vuese:MainList:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|효과음 재생|sound 재생할 사운드|
|moveToChildPage|하위 라우트 페이지로 이동|path 하위 라우트 경로, status 박스 선택 여부|

<!-- @vuese:MainList:methods:end -->


