# SelectedAvatar

아바타 선택한 표정 & 의상으로 사진찍기 페이지

## Methods

<!-- @vuese:SelectedAvatar:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|sendInput|화면 클릭 시, 입력 신호 발송|-|

<!-- @vuese:SelectedAvatar:methods:end -->


## Computed

<!-- @vuese:SelectedAvatar:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|expression|-|서버에서 전달 받은 표정값으로 표정 구현|No|

<!-- @vuese:SelectedAvatar:computed:end -->


