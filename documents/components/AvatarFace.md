# AvatarFace

아바타 표정 컴포넌트

## Props

<!-- @vuese:AvatarFace:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|faceType|-|—|`false`|-|

<!-- @vuese:AvatarFace:props:end -->


## Methods

<!-- @vuese:AvatarFace:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|handleAnimation|애니메이션을 받아서 현재 에니메이션을 교체한다.|aim 애니메이션|
|makeExpression|페이스 타입에 따라 보여줄 현재 얼굴 표정을 설정한다.|-|
|changeFace|3초 뒤에 조는 표정으로 얼굴을 바꾼다.|-|
|toggleNumber|vue.js 에서 화면이 바뀐 것을 알 수 있도록 key 값을 변경한다.|-|

<!-- @vuese:AvatarFace:methods:end -->


