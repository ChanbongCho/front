# AdminFrontPage

설정 첫 화면, 여기에서만 설정완료된 자료를 브릿지로 전송할 수 있다

## Methods

<!-- @vuese:AdminFrontPage:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|confirm|브릿지로 현재 설정 발송|이벤트 객체|
|moveToSetting|설정 페이지로 이동|el 이벤트 객체|
|addAndRemoveActive|활성 클래스 추가 후 제거|el 이벤트 타겟 callback 콜백함수|

<!-- @vuese:AdminFrontPage:methods:end -->


## Computed

<!-- @vuese:AdminFrontPage:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|daySchedule|-|낮 스케쥴이 있으면 '00:00 ~ 00:00', 없으면 스케쥴을 설정해주세요 표시|No|
|nightSchedule|-|야간 스케쥴이 있으면 '00:00 ~ 00:00', 없으면 스케쥴을 설정해주세요 표시|No|
|temperature|-|온도가 있으면 "℃ 이하/이상" "℃ ~℃". 없으면 '기준 값을 설정해주세요'|No|
|patrol|-|활성화된 순찰 스케쥴|No|
|patrolCount|-|활성화된 순찰 횟수|No|

<!-- @vuese:AdminFrontPage:computed:end -->


