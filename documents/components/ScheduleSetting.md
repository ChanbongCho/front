# ScheduleSetting

주얀/아갼/순찰 스케쥴 설정을 담는 부모 페이지

## Methods

<!-- @vuese:ScheduleSetting:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|playSound|소리 재생|sound 소리명|
|onClickGuide|하위 페이지를 주간 스케쥴 페이지로 변경|-|
|onClickSecurity|하위 페이지를 야간 스케쥴 페이지로 변경|-|
|onClickPatrol|하위 페이지를 순찰 스케쥴 페이지로 변경|-|

<!-- @vuese:ScheduleSetting:methods:end -->


