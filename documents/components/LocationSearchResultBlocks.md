# LocationSearchResultBlocks

초성검색 페이지 결과

## Methods

<!-- @vuese:LocationSearchResultBlocks:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|moveToPointingPlace|초성검색 결과 블록 클릭 시, 층별 안내 페이지로 이동해 해당 위치를 표시한다|refName, keyword 해당 결과 블록의 refName 과 값|

<!-- @vuese:LocationSearchResultBlocks:methods:end -->


## Computed

<!-- @vuese:LocationSearchResultBlocks:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|resultBlocks|-|서버에서 받은 결과 배열|No|

<!-- @vuese:LocationSearchResultBlocks:computed:end -->


